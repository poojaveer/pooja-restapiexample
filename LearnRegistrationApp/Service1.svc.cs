﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace LearnRegistrationApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        SqlConnection con = new SqlConnection("Server=ASIA17687;Database=Pooja_Test;Trusted_Connection=True;Persist Security Info=False;User ID=pooja.v;Password=bentley@123;");
        public string InsertCustomerDetails(CustomerDetails customerInfo)
        {
            string strMessage = string.Empty;
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into [Employee]([First Name],[Last Name],[Address]) values(@FName,@LName,@Address)", con);
            cmd.Parameters.AddWithValue("@FName", customerInfo.FirstName);
            cmd.Parameters.AddWithValue("@LName", customerInfo.LastName);
            cmd.Parameters.AddWithValue("@Address", customerInfo.Address);
            int result = cmd.ExecuteNonQuery();
            if (result == 1)
            {
                strMessage = customerInfo.FirstName + " inserted successfully";
            }
            else
            {
                strMessage = customerInfo.FirstName + " not inserted successfully";
            }
            con.Close();
            return strMessage;

        }
        public string InsertName(string name)
        {
            string strMessage = string.Empty;
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into [Employee]([First Name]) values(@FName)", con);
            cmd.Parameters.AddWithValue("@FName", name);
            
            int result = cmd.ExecuteNonQuery();
            if (result == 1)
            {
                strMessage = name + " inserted successfully";
            }
            else
            {
                strMessage = name + " not inserted successfully";
            }
            con.Close();

            return strMessage;
        }
    }
}
